package com.example.rguitou.cascade;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.rguitou.cascade.R.styleable.View;


public class ActiviteJeu extends AppCompatActivity {

    GridView tableDeJeu;
    ArrayList<Item> tempTuiles = new ArrayList<>();
    ArrayList<Item> tuiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activite_jeu);
        initialisation();

        int nbLignesetColonnes = reglageNbColonnes();
        tableDeJeu = (GridView) findViewById(R.id.tableDeJeu);
        tableDeJeu.setNumColumns(nbLignesetColonnes);
        chargementGrille(nbLignesetColonnes);

        Adapteur monAdapteur=new Adapteur(this,R.layout.activity_grid_items, tuiles);
        tableDeJeu.setAdapter(monAdapteur);
        
    }
    
    public int reglageNbColonnes(){

        Intent i = getIntent();
        String rating = i.getStringExtra("diff");
        int nbColonnes = Integer.parseInt(rating);
        
        switch(nbColonnes){
            case 1:
                nbColonnes = 5;
                break;
            case 2:
                nbColonnes = 10;
                break;
            case 3:
                nbColonnes = 15;
                break;
            case 4:
                nbColonnes = 20;
                break;
            case 5:
                nbColonnes = 25;
                break;
        }
        return nbColonnes;
    }

    public void chargementGrille(int nbLignes){
        for(int i = 0; i < nbLignes*nbLignes; i++){
            int indexAleatoire = (int)( Math.random()*( 3 + 1 ) );
            tuiles.add(tempTuiles.get(indexAleatoire));
        }
    }

    public void initialisation(){
        tempTuiles.add(new Item(R.drawable.tuile1));
        tempTuiles.add(new Item(R.drawable.tuile2));
        tempTuiles.add(new Item(R.drawable.tuile3));
        tempTuiles.add(new Item(R.drawable.tuile4));
    }

}
