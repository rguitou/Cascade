package com.example.rguitou.cascade;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    public final String DIFF = "diff";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RatingBar s = (RatingBar) findViewById(R.id.difficulte);
        s.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override public void onRatingChanged(RatingBar ratingBar, float rating,
                                                  boolean fromUser) {
                if(rating<1.0f)
                    ratingBar.setRating(1.0f);
            }
        });


        Button b = (Button) findViewById(R.id.lancementJeu);
    }

    public void lancerJeu(View v){
        RatingBar s = (RatingBar) findViewById(R.id.difficulte);
        Intent i = new Intent(MainActivity.this , ActiviteJeu.class);
        i.putExtra(DIFF, String.valueOf((int)s.getRating()));
        startActivity(i);
    }

}
