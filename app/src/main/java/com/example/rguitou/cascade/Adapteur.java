package com.example.rguitou.cascade;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import java.util.ArrayList;

/**
 * Created by rguitou on 08/03/18.
 */

public class Adapteur extends ArrayAdapter{


    ArrayList<Item> tuiles = new ArrayList<>();

    public Adapteur(Context context, int textViewResourceId, ArrayList objects) {
        super(context, textViewResourceId, objects);
        tuiles = objects;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        v = inflater.inflate(R.layout.activity_grid_items, null);
        ImageView imageView =  (ImageView) v.findViewById(R.id.imageView);
        imageView.setImageResource(tuiles.get(position).getImage());
        return v;

    }
}
